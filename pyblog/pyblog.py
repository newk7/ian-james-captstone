import requests
import json
import os
import html2text
import argparse

# define argparse stuff. allow user to choose upload or read

parser = argparse.ArgumentParser(description='Provide options \
                                for interacting with Wordpress site')

parser.add_argument('option', choices=['read', 'upload'], help='Read \
                      latest blog post or upload a new post')


parser.add_argument('-f', metavar='<input_file>', help='File \
                                       to upload as new post')


api_credentials = requests.auth.HTTPBasicAuth(os.environ['WP_API_USER'],
                                              os.environ['WP_API_PASSWORD'])
base_url = os.environ['WP_URL']


def read_post_file(file):
    blog_body = ''
    try:
        with open(file) as blog_post:
            lines = blog_post.readlines()
            try:
                title = lines[0]
            except IndexError:
                print('File has no title')
            for line in lines[2:]:
                blog_body = blog_body + line
        return title, blog_body
    except Exception:
        print('#################################################')
        print('###  THE FILENAME YOU ENTERED DOES NOT EXIST  ###')
        print('#################################################')
        new_filename = input('\nPlease enter a valid filename:')
        title, blog_body = read_post_file(new_filename)
        return title, blog_body


def make_post(title, content):
    """ this function will take in the content of
    a post and publish that post """

    url = base_url + '/posts'

    headers = {
        'Content-Type': 'application/json'
    }

    post_content = {
        "title": title,
        "content": content,
        "status": "publish"
    }

    post_content = json.dumps(post_content)

    post_blog = requests.post(url, headers=headers,
                              auth=api_credentials, data=post_content)

    return post_blog.status_code


def latest_post():
    url = base_url + '/posts'

    posts_list = requests.get(url, auth=api_credentials)
    posts_list = posts_list.json()
    latest_post = posts_list[0]
    post_text = latest_post.get('content').get('rendered')
    post_title = latest_post.get('title').get('rendered')
    post_title = html2text.html2text(post_title)
    post_text = html2text.html2text(post_text)
    # print(post_text)

    return post_title, post_text


def print_post(title, content):
    print('Title: \n', title)
    print('\nContent: \n', content)


if __name__ == '__main__':

    options = parser.parse_args()

    if options.option == 'read':
        title, content = latest_post()

        print_post(title, content)
    elif options.option == 'upload':
        if options.f is None:
            print('You must provide a file to upload! \
                 ("upload -f <filename>")')
        else:
            blog_title, blog_body = read_post_file(options.f)
            status_code = make_post(blog_title, blog_body)
            if status_code == 201:
                print('\nNew Post Sucessfully Created\n')
                title, content = latest_post()

                print_post(title, content)
            elif status_code > 499:
                print('Server not accessible. Please try again later')
            elif status_code > 399:
                print('There was an error with the POST request')
