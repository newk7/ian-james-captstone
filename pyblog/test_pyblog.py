import unittest
import os
import requests
from pyblog import read_post_file, make_post, latest_post


class TestPyblog(unittest.TestCase):

    def test_reading_file_title(self):
        file = 'test.txt'

        title, body = read_post_file(file)
        title = title.strip()

        self.assertEqual('Title', title)

    def test_reading_file_body(self):
        file = 'test.txt'

        title, body = read_post_file(file)
        body = body.strip()
        self.assertEqual('Body Text', body)

    def test_env_vars_set(self):
        self.assertIsNotNone(os.environ['WP_API_USER'],
                             msg='API User Name Environment Varible Not Set')
        self.assertIsNotNone(os.environ['WP_API_PASSWORD'],
                             msg='API Password Environment Varible Not Set')
        self.assertIsNotNone(os.environ['WP_URL'],
                             msg='API URL Environment Varible Not Set')

    def test_API_connection(self):
        url = os.environ['WP_URL']
        api_auth = requests.auth.HTTPBasicAuth(os.environ['WP_API_USER'],
                                               os.environ['WP_API_PASSWORD'])
        api_check = requests.get(url, auth=api_auth)
        self.assertEqual(api_check.status_code, 200)

    def test_API_bad_creds(self):
        url = os.environ['WP_URL']
        api_auth = requests.auth.HTTPBasicAuth('user', 'somepassword')
        api_check = requests.get(url, auth=api_auth)
        self.assertEqual(api_check.status_code, 401)

    def test_make_post(self):
        title, body = read_post_file('test.txt')
        post_check = make_post(title, body)
        self.assertEqual(post_check, 201)

    def test_get_latest(self):
        # check post that was made in previous test is there

        post_title, post_body = latest_post()
        post_title = post_title.strip()
        post_body = post_body.strip()
        self.assertEqual(post_title, "Title")
        self.assertEqual(post_body, "Body Text")
